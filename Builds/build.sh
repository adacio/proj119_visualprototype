GROWL="osascript -e"
PRODUCT_NAME=“VisualPrototype”

${GROWL} 'display notification "Building Unity Project" with title "Adacio"'
/Applications/Unity/Unity.app/Contents/MacOS/Unity -quit -batchmode -executeMethod AutoBuilder.PerformiOSBuild

cd /Users/justinheidenreich/Desktop/Unity\ Projects/Proj119_Visual_Prototype/Builds/iOS

${GROWL} 'display notification "Creating IPA" with title "Adacio"'
xcodebuild -scheme Unity-iPhone archive -archivePath /Users/justinheidenreich/Desktop/Unity\ Projects/Proj119_Visual_Prototype/Builds/iOS/Unity-iPhone.xcarchive

${GROWL} 'display notification "Exporting Archive" with title "Adacio"'
rm "/Users/justinheidenreich/Desktop/Unity Projects/Proj119_Visual_Prototype/Builds/iOS/VProto1.ipa"

xcodebuild -exportArchive -exportFormat ipa -archivePath "/Users/justinheidenreich/Desktop/Unity Projects/Proj119_Visual_Prototype/Builds/iOS/Unity-iPhone.xcarchive" \
	-exportPath "/Users/justinheidenreich/Desktop/Unity Projects/Proj119_Visual_Prototype/Builds/iOS/VProto1.ipa" \
	-exportProvisioningProfile "Visual Prototype 1"

${GROWL} 'display notification "Uploading Archive" with title "Adacio"'
curl https://rink.hockeyapp.net/api/2/apps/upload -H "X-HockeyAppToken: 7abc7bfa75fc4d1b9659acb9663c235d" \
     -F "status=2" \
     -F "notify=0" \
     -F "notes=This build was uploaded via the upload API." \
     -F "notes_type=0" \
     -F "ipa=@/Users/justinheidenreich/Desktop/Unity Projects/Proj119_Visual_Prototype/Builds/iOS/VProto1.ipa"


${GROWL} 'display notification "Opening Build List" with title "Adacio"'
/usr/bin/open "https://rink.hockeyapp.net/manage/apps/162554"
